package rabbitmq

import "github.com/rabbitmq/amqp091-go"

type Worker interface {
	// Function to be implemented by all Workers. Will call from polymorphism mecanism
	Consume(channel *amqp091.Channel, id int)
}
