package types

import (
	"time"

	"github.com/rabbitmq/amqp091-go"
)

type Exchange struct {
	Name       string
	Type       string
	Durable    bool
	AutoDelete bool
	Internal   bool
	NoWait     bool
	Args       amqp091.Table
	Declare    bool
}

type Queue struct {
	Name          string
	Durable       bool
	AutoDelete    bool
	Exclusive     bool
	NoWait        bool
	Args          amqp091.Table
	RoutingKey    string
	BindExchanges *[]Exchange
	Declare       bool
}

type RetryConfig struct {
	RetryQueue    *Queue
	RetryExchange *Exchange
	RetryTimeout  int64
	RetryCount    int64
	ErrorQueue    *Queue
	ErrorExchange *Exchange
}

type BaseConfig struct {
	Exchange    *Exchange
	Queue       *Queue
	Retryable   bool
	RetryConfig *RetryConfig
}

type Reconnect struct {
	MaxAttempt int
	Interval   time.Duration
}

type ErrorMessage struct {
	Error       string      `json:"error"`
	Reason      string      `json:"reason"`
	Payload     interface{} `json:"payload"`
	FailedAt    time.Time   `json:"failedAt"`
	Properties  interface{} `json:"properties"`
	NumAttempts int64       `json:"numAttempts"`
}
