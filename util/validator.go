package util

import (
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/linear-packages/go/rabbitmq/types"
)

type Validator struct{}

func (v *Validator) CheckExchanges(chn *amqp091.Channel, exchanges []types.Exchange) error {
	for _, ex := range exchanges {
		if err := chn.ExchangeDeclarePassive(
			ex.Name,
			ex.Type,
			ex.Durable,
			ex.AutoDelete,
			ex.Internal,
			ex.NoWait,
			ex.Args,
		); err != nil {
			return err
		}
	}
	return nil
}
