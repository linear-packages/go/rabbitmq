package rabbitmq

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"github.com/rabbitmq/amqp091-go"
	lgr "gitlab.com/linear-packages/go/logger"
	secret "gitlab.com/linear-packages/go/secret-manager"
	"gitlab.com/linear-packages/go/sincronizador-utils/functions"
)

var logger = lgr.New("main")

type RabbitConfig struct {
	Schema         string `json:"schema"`
	Username       string `json:"userName"`
	Password       string `json:"password"`
	Host           string `json:"host"`
	Port           string `json:"port"`
	VHost          string `json:"vHost"`
	ConnectionName string
}

type Rabbit struct {
	config     RabbitConfig
	connection *amqp091.Connection
}

// NewRabbit returns a instance of Rabbit
func NewRabbit(secretKey *string, config *RabbitConfig) (*Rabbit, error) {
	c, err := getConfig(secretKey, config)
	if err != nil {
		return nil, err
	}
	return &Rabbit{config: *c}, nil
}

func getConfig(secretKey *string, config *RabbitConfig) (*RabbitConfig, error) {
	if config != nil {
		return config, nil
	}

	sm := secret.NewSecretManager()
	v, err := sm.GetValue(*secretKey)
	if err != nil {
		return nil, err
	}

	c := &RabbitConfig{}
	if err = json.Unmarshal([]byte(*v), c); err != nil {
		return nil, err
	}
	c.ConnectionName = os.Getenv("APPLICATION_NAME")
	return c, nil
}

// Connect establish a connection with a rabbitmq server and set a amqp091.Connection instance
// to Rabbit.connection
func (r *Rabbit) Connect() error {
	if r.connection == nil || r.connection.IsClosed() {
		con, err := amqp091.DialConfig(fmt.Sprintf(
			"%s://%s:%s@%s:%s/%s",
			r.config.Schema,
			r.config.Username,
			r.config.Password,
			r.config.Host,
			r.config.Port,
			r.config.VHost,
		), amqp091.Config{Properties: amqp091.Table{"connection_name": r.config.ConnectionName}})
		if err != nil {
			return err
		}
		r.connection = con
	}

	return nil
}

func (r *Rabbit) ConnectWithRetries() error {
	configFuncConnect := functions.RetryableFuncParameters{
		Function:   r.Connect,
		SuccessMsg: "Conexão com o rabbitmq estabelecida com sucesso.",
		ErroMsg:    "Falha ao abrir conexão com rabbitmq.",
	}
	err := functions.CallRetryableFunc(configFuncConnect)
	if err != nil {
		logger.Error("Tentativas esgotadas não foi possível abrir conexão com rabbitmq.", err)
		return err
	}

	return nil
}

// Connection returns exiting `*amqp091.Connection` instance.
func (r *Rabbit) Connection() (*amqp091.Connection, error) {
	if r.connection == nil || r.connection.IsClosed() {
		return nil, errors.New("connection is not open")
	}

	return r.connection, nil
}

// Channel returns a new `*amqp091.Channel` instance.
func (r *Rabbit) Channel() (*amqp091.Channel, error) {
	chn, err := r.connection.Channel()
	if err != nil {
		return nil, err
	}

	return chn, nil
}
