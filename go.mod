module gitlab.com/linear-packages/go/rabbitmq

go 1.20

require (
	gitlab.com/linear-packages/go/secret-manager v0.0.1
)

require (
	github.com/rabbitmq/amqp091-go v1.9.0 // indirect
	gitlab.com/linear-packages/go/logger v0.0.1 // indirect
)

require (
	github.com/aws/aws-sdk-go v1.42.4 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	gitlab.com/linear-packages/go/sincronizador-utils v0.0.16
)
